import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { ToasterService } from '../toaster.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})

export class LoginComponent implements OnInit {
  userName: string;
  emailId: string;
  passWord: string;
  retypepassWord: string;
  signingUp: boolean = false;
  isAdminUser: boolean = false;
  adminExist: boolean = true;
  adminUserName: string;
  adminPassWord: string;
  forgetPasswordflag: boolean = false;
  constructor(private _service: LoginService, private _toaster: ToasterService, private _router: Router) {

  }


  ngOnInit() {
    this._service.checkIfAdminExist().subscribe(isExist => {
      this.adminExist = isExist;
      if (isExist) {
        this.isAdminUser = false;
      }
    })
  }

  addUser() {
    if (this.emailId == undefined || this.userName == undefined || this.passWord == undefined || this.retypepassWord == undefined) {
      this._toaster.error('ERROR', "Please Enter Valid Details");
    } else if (this.passWord !== this.retypepassWord) {
      this._toaster.error('ERROR', "Password Does Not Match.");
    } else if (!this.adminExist && !this.isAdminUser) {
      this._toaster.error('ERROR', 'The first User should be Admin');
    } else {
      const params = { "emailId": btoa(this.emailId), "password": btoa(this.passWord), 'userName': btoa(this.userName), "role": this.isAdminUser ? "ADMIN" : "USER" };
      this._service.addUser(params).subscribe(result => {
        if (result) {
          if (!this.adminExist && this.isAdminUser) {
            this.adminExist = true;
            this.isAdminUser = false;
          }
          this._toaster.info("Info", "User added Successfully");
          this.passWord = "";
          this.userName = "";
          this.emailId = "";
          this.retypepassWord = "";
          this.signingUp = false;
        } else {
          this._toaster.error('ERROR', "User already Existing Choose Another Email");
          this.emailId = "";
        }
      });

    }
  }


  forgetPassword() {
    this.forgetPasswordflag = true;
  }

  onForgetPasswordSubmit() {
    if (!this.emailId || !this.passWord
      || !this.adminUserName || !this.adminPassWord) {
      this._toaster.error('ERROR', "Please Enter all Required Fiedls");
    } else {
      const params = { "userEmailId": btoa(this.emailId), "userPassword": btoa(this.passWord), "adminEmailId": btoa(this.adminUserName), "adminPassword": btoa(this.adminPassWord) };
      this._service.changePassword(params).subscribe(changePasswordResponse => {
        if (changePasswordResponse.result) {
          this.adminUserName = "";
          this.adminPassWord = "";
          this.forgetPasswordflag = false;
          this._toaster.success('', 'Password Updated. Login to proceed.');
        } else {
          this._toaster.error('ERROR', changePasswordResponse.message);
        }
      })
    }
  }

  onClickSubmit(event) {
    const params = { "userName": btoa(this.emailId), "password": btoa(this.passWord) };
    if (this.emailId == undefined) {
      this._toaster.error('ERROR', "Please Check your User Name or Password");
    } else {
      this._service.getUsers(params).pipe(catchError(error => {
        if (error.status === 401) {
          this._toaster.error('ERROR', "Bad Credentials!... Please try later");
          this.signingUp = false;
        } else {
          this._toaster.error('ERROR', "Server Error!... Please try later");
          this.signingUp = false;
          this.passWord = "";
          this.userName = "";
          this.emailId = "";
        }
        return null;
      })).subscribe(result => {
        sessionStorage.setItem('jwtToken', result['jwt']);
        sessionStorage.setItem('userName', result['userName']);
        this._router.navigateByUrl("home");
      });
    }
  }

}
