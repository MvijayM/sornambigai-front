import { ToasterService } from './../../toaster.service';
import { BillHeaderModel } from './../models/bill-calcuation-models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-customer-name-input-dialog',
  templateUrl: './customer-name-input-dialog.component.html',
  styleUrls: ['./customer-name-input-dialog.component.css']
})
export class CustomerNameInputDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CustomerNameInputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: BillHeaderModel, private _toaster: ToasterService) { }

  ngOnInit() {
  }

  onClose(confirmation:boolean) {
    
    if(confirmation && (this.data.customerName ==="" || !this.data.customerName)) {
      this._toaster.error('ERROR','Please Enter Customer Name');
    } else {
      this.dialogRef.close(confirmation);
    }
  }

}
