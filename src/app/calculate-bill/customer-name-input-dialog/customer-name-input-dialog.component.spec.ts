import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerNameInputDialogComponent } from './customer-name-input-dialog.component';

describe('CustomerNameInputDialogComponent', () => {
  let component: CustomerNameInputDialogComponent;
  let fixture: ComponentFixture<CustomerNameInputDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerNameInputDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerNameInputDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
