import { InformationDisplayDialogComponent } from './../common-component/information-display-dialog/information-display-dialog.component';
import { FileDownloadUtil } from './../common-component/file-download-utils';
import { CustomerNameInputDialogComponent } from './customer-name-input-dialog/customer-name-input-dialog.component';
import { ExpandedItemGridComponent } from './expand-grid/expanded-item-grid.component';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from './../common-component/confirmation-dialog/confirmation-dialog.component';
import { ToasterService } from './../toaster.service';
import { startWith, map } from 'rxjs/operators';
import { ItemModel } from './../add-jewell/add-jewell.component';
import { FormControl } from '@angular/forms';
import { BillManagementService, PriceModel } from './bill-management-service';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { CustomDataSource } from './grid-datasource/grid-datasource';
import { ItemDetailModel, BillCalcuationRequestModel, BillHeaderModel } from './models/bill-calcuation-models';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-calculate-bill',
  templateUrl: './calculate-bill.component.html',
  styleUrls: ['./calculate-bill.component.css'],
  providers: [BillManagementService]
})
export class CalculateBillComponent implements OnInit {
  displayedColumns: string[] = ['itemId', 'item', 'itemCategory', 'costPerGram', 'makingCharge',
    'gramsPurchased', 'wastage', 'quantity', 'itemAmount', 'delete'];
  itemCategoryArray: string[];
  sortBy: any;
  isEditDisabled:boolean = false;
  itemCategoryTable: any[];
  billId: string;
  items: any[] = [];
  totalAmount: number = 0;
  isValid: boolean = true;
  itemCategoryId: string;
  gramsPurchased: number;
  wastage: number = 0;
  customerName: string = "";
  makingCharge: number = 0;
  cashierName: string = sessionStorage.getItem('userName');
  itemName: string;
  itemToBeDelted: string;
  itemPrice: number = 0;
  quantity: number = 0;
  itemControl = new FormControl();
  itemArray: ItemDetailModel[] = [];
  filteredItems: Observable<ItemModel[]>;
  prices: PriceModel[] = [];
  itemCategoryFormControl = new FormControl();
  dataSourceSubject: BehaviorSubject<ItemDetailModel[]> = new BehaviorSubject<ItemDetailModel[]>(this.itemArray);
  dataSource = new CustomDataSource(this.dataSourceSubject);

  constructor(private _service: BillManagementService,
    private _toaster: ToasterService, public dialog: MatDialog,
    private route: ActivatedRoute, private router: Router) {
    this.filteredItems = this.itemControl.valueChanges
      .pipe(
        startWith(''),
        map(state => this._filterItem(state))
      );
    this.itemCategoryFormControl.valueChanges.subscribe(itemCategoryId => {
      this.itemCategoryId = itemCategoryId;
      this.setPrice();
      this.loadDisplayItems('');
    })
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(queryParams => {
      if (queryParams['billId']) {
        this.billId = queryParams['billId'];
      };
    })
    forkJoin(this._service.getItemCategories(), this._service.getPriceList(),
      this._service.getBillByBillId(this.billId)).subscribe(res => {
        this.itemCategoryTable = res[0];
        this.prices = res[1];
        this.itemCategoryArray = this.itemCategoryTable.map(itemCategory => itemCategory.itemCategoryName);
        this.itemCategoryArray = Array.from(new Set(this.itemCategoryArray));
        if (res[0] && res[0].length > 0) {
          this.itemCategoryId = this.itemCategoryTable[0].itemCategoryId;
          this.itemCategoryFormControl.patchValue(this.itemCategoryId);
        } else {
          this._toaster.error('ERROR', 'Item Category not fetched. Sorry for the inconvenience')
        }
        if (this.prices.length === 0) {
          this._toaster.error('ERROR', 'Prices not fetched!!!. Sorry for the inconvenience.');
          this.itemPrice = 0;
        } else {
          this.setPrice();
        }
        this.loadDisplayItems('')
        if (res[2]) {
          this.totalAmount = res[2].totalBillAmount;
          this.customerName = res[2].customerName;
          this.itemArray = res[2].items;
          this.isEditDisabled = res[2].billStatus === 'CLOSED';
          if(res[2].billStatus !=='CLOSED') {
            this.dialog.open(InformationDisplayDialogComponent,{
              width:'300px',
              data: {
                content:"As Bill is not closed, Item's Prices are updated to today's Price."
              }
            });
          }
          this.dataSourceSubject.next(this.itemArray);
        }
      });
  }

  deleteBill() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '250px',
      data: {
        content: 'Are you sure, you want to permanently discard this Bill?',
        type: 'warn'
      }
    });
    dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        if (this.billId) {
          this._service.deleteByBillId(this.billId).subscribe(result => {
            if (result) {
              this.billId = "";
              this.totalAmount = 0;
              this.itemArray = [];
              this.dataSourceSubject.next(this.itemArray);
              this._toaster.success('', 'Bill Deleted Permanently');
              this.router.navigate(['../calculateBill'],
                { relativeTo: this.route })
            }
          });
        } else {
          this.itemArray = [];
          this.totalAmount = 0;
          this.dataSourceSubject.next(this.itemArray);
        }
      }
    });
  }

  saveBill() {
    if (this.itemArray.length == 0) {
      this._toaster.error('ERROR', 'Nothing to save. Please Add items to Bill');
    } else {
      let billHeader: BillHeaderModel = {
        billId: this.billId,
        totalBillAmount: this.totalAmount,
        billStatus: 'DRAFT',
        customerName: this.customerName,
        cashierName: this.cashierName,
        items: this.itemArray
      }
      const dialogRef = this.dialog.open(CustomerNameInputDialogComponent, {
        width: '300px',
        data: billHeader
      });
      dialogRef.disableClose = true;
      dialogRef.afterClosed().subscribe(customerName => {
        if (customerName !== false) {
          this._service.saveBill(billHeader).subscribe(result => {
            if (result) {
              this.billId = result.billId;
              this._toaster.success('', 'Bill Saved');
            } else {
              this._toaster.error('ERROR', 'Bill not saved. Sorry for the inconvenience.');
            }
          });
        }
      });
    }
  }

  closeBill() {
    if (this.itemArray.length === 0) {
      this._toaster.error('ERROR', "No Items Purchased. Can't Close Bill.");
    } else {
      const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '250px',
        data: {
          content: 'Are you sure you want to close the Bill? Once closed, the Bill cannot be edited'
        }
      });
      confirmationDialogRef.afterClosed().subscribe(confirmation => {
        if (confirmation) {
          let billHeader: BillHeaderModel = {
            billId: this.billId,
            totalBillAmount: this.totalAmount,
            billStatus: 'CLOSED',
            customerName: this.customerName,
            cashierName: this.cashierName,
            items: this.itemArray
          }
          const dialogRef = this.dialog.open(CustomerNameInputDialogComponent, {
            width: '300px',
            data: billHeader
          });
          dialogRef.disableClose = true;
          dialogRef.afterClosed().subscribe(customerName => {
            if (customerName !== false) {
              this._service.closeAndExportBill(billHeader).subscribe(fileModel => {
                if (fileModel.result) {
                  this.billId = fileModel.billId;
                  this.isEditDisabled = true;
                  FileDownloadUtil.downloadFile(fileModel.fileData,fileModel.fileName);
                } else {
                  this._toaster.warning('WARNING',fileModel.failMessage);
                }
              });
            }
          });
        }
      });
    }
  }

  printBill() {
    this._service.printBill(this.billId).subscribe(fileModel =>{
      if(fileModel.result){
      FileDownloadUtil.downloadFile(fileModel.fileData,fileModel.fileName);
      } else {
        this._toaster.error('ERROR',fileModel.failMessage);
      }
    });
  }
  checkValidationAndAdd() {
    if (!this.itemCategoryId || !this.itemControl.value
      || !this.gramsPurchased || this.gramsPurchased === 0
      || this.quantity === 0) {
      this._toaster.error('ERROR!', 'Please Check the contents');
    } else {
      const existingItem = this.itemArray.find(item => (item.itemId === this.itemControl.value && item.gramsPurchased === this.gramsPurchased));
      if (existingItem) {
        this.doExistItemBill(existingItem);
      } else {
        const itemDetails: ItemDetailModel = {
          itemId: this.itemControl.value,
          itemName: this.items.find(item => item.itemId === this.itemControl.value).itemName,
          itemCategoryId: this.itemCategoryId,
          pricePerGramOnPurchase: this.itemPrice,
          gramsPurchased: this.gramsPurchased,
          itemAmount: 0,
          quantity: this.quantity,
          wastage: this.wastage,
          makingCharge: this.makingCharge
        }
        const params: BillCalcuationRequestModel = {
          billHeader: {
            billId: this.billId,
            totalBillAmount: this.totalAmount,
            billStatus: 'OPEN',
            customerName: this.customerName,
            cashierName: this.cashierName,
            items: this.itemArray
          },
          itemDetails: itemDetails
        };
        this._service.addItemToBilAfterCalculation(params).subscribe(result => {
          this.itemArray.push(result.itemDetails);
          this.totalAmount = result.totalAmount;
          this.itemArray = this.itemArray.sort((item1, item2) => item1.itemId - item2.itemId);
          this.dataSourceSubject.next(this.itemArray);
        });
      }
    }
  }

  doExistItemBill(existingItem) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '250px',
      data: { content: 'Item Already Exists. Do you want to replace it? If No It will increase the quantity of the current Item.' }
    });
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(confirmation => {
      this.itemArray = this.itemArray.filter(item => item !== existingItem);
      if (!confirmation) {
        const itemDetails: ItemDetailModel = {
          itemId: this.itemControl.value,
          itemName: this.items.find(item => item.itemId === this.itemControl.value).itemName,
          itemCategoryId: this.itemCategoryId,
          pricePerGramOnPurchase: this.itemPrice,
          gramsPurchased: this.gramsPurchased,
          itemAmount: 0,
          quantity: this.quantity + existingItem.quantity,
          wastage: this.wastage,
          makingCharge: this.makingCharge
        }
        const params: BillCalcuationRequestModel = {
          billHeader: {
            billId: this.billId,
            totalBillAmount: this.totalAmount - existingItem.itemAmount,
            billStatus: 'OPEN',
            customerName: this.customerName,
            cashierName: this.cashierName,
            items: this.itemArray
          },
          itemDetails: itemDetails
        };
        this._service.addItemToBilAfterCalculation(params).subscribe(result => {
          this.itemArray.push(result.itemDetails);
          this.totalAmount = result.totalAmount;
          this.itemArray = this.itemArray.sort((item1, item2) => item1.itemId - item2.itemId);
          this.dataSourceSubject.next(this.itemArray);
        })
      } else {
        const itemDetails: ItemDetailModel = {
          itemId: this.itemControl.value,
          itemName: this.items.find(item => item.itemId === this.itemControl.value).itemName,
          itemCategoryId: this.itemCategoryId,
          pricePerGramOnPurchase: this.itemPrice,
          gramsPurchased: this.gramsPurchased,
          itemAmount: 0,
          quantity: this.quantity,
          wastage: this.wastage,
          makingCharge: this.makingCharge
        }
        const params: BillCalcuationRequestModel = {
          billHeader: {
            billId: this.billId,
            totalBillAmount: this.totalAmount - existingItem.itemAmount,
            billStatus: 'OPEN',
            customerName: this.customerName,
            cashierName: this.cashierName,
            items: this.itemArray
          },
          itemDetails: itemDetails
        };
        this._service.addItemToBilAfterCalculation(params).subscribe(result => {
          this.itemArray.push(result.itemDetails);
          this.totalAmount = result.totalAmount;
          this.itemArray = this.itemArray.sort((item1, item2) => item1.itemId - item2.itemId);
          this.dataSourceSubject.next(this.itemArray);
        });
      }
    })
  }

  setPrice() {
    if (!this.itemCategoryId || this.prices.length === 0) {
      this.itemPrice = 0;
    } else if (!this.prices.find(priceModel => priceModel.itemCategoryId === this.itemCategoryId)) {
      this.itemPrice = 0
    } else {
      this.itemPrice = this.prices.find(priceModel => priceModel.itemCategoryId === this.itemCategoryId).pricePerGram
    }
    if (this.itemPrice === 0) {
      this._toaster.warning('WARNING!', 'Price set to 0. Be careful');
    }
  }

  expandGrid() {
    this.dialog.open(ExpandedItemGridComponent, {
      width: '1500px',
      height: 'fit-content',
      data: {
        dataSource: this.dataSource,
        itemCategoryTable: this.itemCategoryTable,
        totalAmount: this.totalAmount
      }
    });
  }

  getItemCategoryName(itemCategoryId) {
    if (this.itemCategoryTable && itemCategoryId) {
      return this.itemCategoryTable.find(itemCategory => itemCategoryId === itemCategory.itemCategoryId).itemCategoryName;
    } else {
      return "";
    }
  }

  /** Gets the total cost of all transactions. */
  getTotalCost() {
    return this.totalAmount;
  }

  emitDeletedId(itemId, gramsPurchased) {
    const existingItem = this.itemArray.find(item => item.itemId === itemId && item.gramsPurchased === gramsPurchased);
    this.itemArray = this.itemArray.filter(item => item !== existingItem);
    this.totalAmount -= existingItem.itemAmount;
    this.dataSourceSubject.next(this.itemArray);
  }

  loadDisplayItems(keyword) {
    const param = { 'itemCategoryId': this.itemCategoryId, 'keyword': keyword }
    this._service.loadItems(param).subscribe(items => {
      if (items.length > 0) {
        this.items = items;
      } else {
        this.items = [];
        this.isValid = false;
      }
      this.itemControl.markAsTouched();
      this.itemControl.patchValue('');
    });
  }

  clearIfInvalid() {
    if (!this.items.find(item => item.itemId === this.itemControl.value)) {
      this.itemControl.patchValue('');
    }
  }

  getDisplayName(itemId) {
    if (!itemId) {
      return "";
    }
    if (typeof (itemId) !== 'number') {
      return "";
    }
    return this.items.find(item => item.itemId === itemId).itemName;
  }

  private _filterItem(value: string): ItemModel[] {
    let filterValue = ''
    if (value && typeof (value) === 'string') {
      filterValue = value.toLowerCase();
    } else if (typeof (value) === 'number') {
      this.isValid = true;
    }

    if (typeof (value) === 'string') {
      this.isValid = this.items.filter(item => item.itemName === filterValue).length > 0;
      return this.items.filter(item => item.itemName.toLowerCase().indexOf(filterValue) === 0);
    } else {
      return [];
    }

  }

}
